#!/usr/bin/env node

const { execSync } = require('child_process');
const path = require('path');
const fs = require('fs-extra');

const projectName = process.argv[2] || 'my-geogirafe-app';
const projectPath = path.join(process.cwd(), projectName);

// Function to check if git is available
function checkGit() {
  try {
    execSync('git --version', { stdio: 'ignore' });
    return true;
  } catch (error) {
    return false;
  }
}

if (fs.existsSync(projectPath)) {
  console.error("\x1b[31m", `Directory ${projectName} already exists!`);
  process.exit(1);
}

if (!checkGit()) {
  console.error("\x1b[31m", 'Error: Git is not installed or not available in PATH.');
  process.exit(1);
}

try {
  console.log("\x1b[32m", '=> Creating the application...', "\x1b[0m");
  execSync(`git clone https://gitlab.com/geogirafe/gg-sample.git ${projectPath}`, { stdio: 'inherit' });
  fs.removeSync(path.join(projectPath, '.git'));
  fs.removeSync(path.join(projectPath, 'package-lock.json'));

  console.log("\x1b[32m", '=> Installing dependencies...');
  console.log("\x1b[32m", 'It may take 2 or 3 minutes, just long enough to get a coffee :-)', "\x1b[0m");
  execSync(`cd ${projectPath} && npm install`, { stdio: 'inherit' });

  console.log("\x1b[32m", '=> Creating default templates...');
  fs.copySync(path.join(projectPath, 'node_modules', '@geogirafe', 'lib-geoportal', 'templates'), projectPath);
  
  console.log("\x1b[32m", '=> Done!');
  console.log("\x1b[32m", `The project ${projectName} has been created.`);
  console.log("\x1b[32m", `You can now start by reading the README file.`);
  console.log("\x1b[32m", `Enjoy!`, "\x1b[0m");

} catch (error) {
  console.error("\x1b[31m", `=> Error during project setup: ${error.message}`);
  console.error("\x1b[31m", `Don't panic, you can get help on the Discord of GeoGirafe: https://discord.com/channels/1194934479282778122`, "\x1b[0m");
  process.exit(1);
}
